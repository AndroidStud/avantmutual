# AvantMutual

This application finds weekdays between two dates.
It consumes local service "MyService" which fetches the public holiday from database.

No. of weekdays are calculated by using below algorithm.
/**
 * Algorithm: 1. First we find the no of days between From and To date.
 * 2. As we have to exclude selected date, we would subtract 2 from the total days.
 * 3. Our whole logic is based on To date, so we would save To date in seperate calendar object.
 * 4. If total no of days is completely divisible by 7, then atleast it has one weekend (Saturday/Sunday)
 * 5. Else if total days is greater than 6, then calculating till multiple of 7 and using quotient to find rest of the dates
 * for which we have to find whether the dates are on Saturday/Sunday.
 * 6. Say, total days are 20, then there are atleast 10 working days and we have to search days for last 6 dates.
 * 7. If the rest of the days are weekdays, then add day in total weekdays.
 * 8 If total no of days is less than 7, then quotient is the weekdays when divided by 7.
 */

Cover all public holidays.
/**
 * 1. Fixed dates(fixedHoliday) -> Anzac day (25/4)
 * 2. Dynamic dates/Monday holidays(allMondayHoliday) -> Public holidays when they fall on weekends, so next Monday becomes holiday.
 * New year day (01/01)
 * Australia day (26/01)
 * Christmas day (25/12)
 * Boxing day (26/12)
 * 3. Random public holidays(dynamicHoliday) -> Which falls on specific day in a month like 1st monday or 2nd monday.
 * Queens birthday(2nd monday in june)
 * Labour day(1st monday in october)
 * Except easter holidays rest are handled as Eater depends on astronomical events that.
 */

Logic to exclude public holidays is as follows:

1. As we have the list of all holidays we can run a loop to find if holiday is a Weekday or Weekend.
2. If it is a weekday, it will get deducted from the total no of weekdays, provided it lies in From and To date range.

//Note: What not covered?
1. Numeric year in edittext. Currently, it can accept anything.
2. Keyboard not automatic hiding when user clicks on "Get Holiday" button.
