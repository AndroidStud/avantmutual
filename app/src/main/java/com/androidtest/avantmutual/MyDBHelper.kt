package com.androidtest.avantmutual

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

/**
 * This class is Database helper class which inserts the holidays in database and fetches them.
 */
class MyDBHelper(m_ctx: Context) : SQLiteOpenHelper(m_ctx, DATABASE_NAME, null, DATABASE_VERSION) {

    /**
     * Called as soon as the Database class is instantiated.
     * It creates the table and insert pre defined data in database.
     */
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_QUERY)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    }

    /**
     * Get public holidays from the database and return list to service class.
     */
    fun getHolidayList(ctx: Context): List<String> {
        val mStrings = ArrayList<String>()

        var dbh: MyDBHelper? = null
        var db: SQLiteDatabase? = null
        try {
            dbh = MyDBHelper(ctx)
            db = dbh.writableDatabase

            val cInfo = db!!.rawQuery(
                    SELECT_QUERY, null)
            if (cInfo != null) {
                while (cInfo.moveToNext()) {
                    mStrings.add(cInfo.getString(cInfo.getColumnIndex(INFO_TEXT_COLUMN)))
                }
            }
            cInfo?.close()
        } catch (e: Throwable) {
            Log.e(TAG, "getHolidayList: Caught - " + e.javaClass.name, e)
        } finally {
            db?.close()
            dbh?.close()
        }
        return mStrings
    }

    /**
     * Insert holiday data from arraylist to database.
     */
    fun insert(concatenatedHolidayList: ArrayList<String>?, ctx: Context) {
        var dbh: MyDBHelper? = null
        var db: SQLiteDatabase? = null
        try {
            dbh = MyDBHelper(ctx)
            db = dbh.writableDatabase
            for (i in 0 until concatenatedHolidayList!!.size) {
                val cv = ContentValues()
                cv.put(INFO_TEXT_COLUMN, concatenatedHolidayList[i])
                db.insertOrThrow(TABLE_NAME, null, cv)
            }
        } catch (e: Throwable) {
            Log.e(TAG, "insert: Caught - " + e.javaClass.name, e)
        } finally {
            db?.close()
            dbh?.close()
        }
    }

    /**
     *  Delete holiday data from table in database.
     */
    fun delete(ctx: Context) {
        var dbh: MyDBHelper? = null
        var db: SQLiteDatabase? = null
        try {
            dbh = MyDBHelper(ctx)
            db = dbh.writableDatabase
            db.delete(TABLE_NAME, null, null)
        } catch (e: Throwable) {
            Log.e(TAG, "delete: Caught - " + e.javaClass.name, e)
        } finally {
            db?.close()
            dbh?.close()
        }
    }

    companion object {
        private const val TAG = "MyDBHelper"
        private const val DATABASE_NAME = "test.db"
        private const val TABLE_NAME = "TESTING"
        private const val DATABASE_VERSION = 1
        private const val CREATE_QUERY = "CREATE TABLE IF NOT EXISTS TESTING (INFO_TEXT TEXT);"
        private const val SELECT_QUERY = "select INFO_TEXT from TESTING;"
        private const val INFO_TEXT_COLUMN = "INFO_TEXT"
    }
}
