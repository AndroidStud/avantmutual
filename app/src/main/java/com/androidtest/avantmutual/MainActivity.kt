package com.androidtest.avantmutual

import android.app.DatePickerDialog
import android.app.ListActivity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.text.InputType
import android.view.View
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*

import java.util.ArrayList
import java.util.Calendar
import java.util.concurrent.TimeUnit

/**
 * This is the main file which shows a list view for public holidays and calculate no of weekdays between dates.
 * Note: What not covered?
 * 1. Numeric year in edittext. Currently, it can accept anything.
 * 2. Keyboard not automatic hiding when user clicks on "Get Holiday" button.
 */
class MainActivity : ListActivity(), ServiceConnection {

    /**
     * myService: This is a local service running in background attached to this activity.
     * It helps in accessing database to get public holidays.
     * datePickerDialog: This shows android default date/calendar dialog.
     * calendarFrom,calendarTo: They represents the date from/to from datePickerDialog.
     * calendarToCalculate: This is responsible for calculating no of weekends between dates.
     * arrayAdapter: Android default adapter accepting string values.
     * holidayList: List of all public holidays.
     * weekdays: Final result showing no of weekdays.
     * year: year selected by user.
     */
    private var myService: MyService? = null
    private var datePickerDialog: DatePickerDialog? = null
    private var calendarFrom: Calendar? = null
    private var calendarTo: Calendar? = null
    private var calendarToCalculate = Calendar.getInstance()
    private var arrayAdapter: ArrayAdapter<String>? = null
    private var holidayList: MutableList<String>? = null
    private var weekdays: Int = 0
    private var year: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setEditTextValueAndType()
        setDataBase()
        setAdapter()
    }

    private fun setEditTextValueAndType() {
        val cal = Calendar.getInstance()
        editTextYear.setText(cal.get(Calendar.YEAR).toString())

        editTextFrom.inputType = InputType.TYPE_NULL
        editTextTo.inputType = InputType.TYPE_NULL
    }

    /**
     * Call database constructor through context.
     */
    private fun setDataBase() {
        MyDBHelper(applicationContext)
    }

    /**
     * Set list adapter to accept array of holidays defined as Strings.
     */
    private fun setAdapter() {
        holidayList = ArrayList()
        this.arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1, holidayList!!)
        this.listAdapter = arrayAdapter
    }

    /**
     * Bind MyService class to this activity when it resumes.
     */
    override fun onResume() {
        super.onResume()
        val intent = Intent(this, MyService::class.java)
        bindService(intent, this, Context.BIND_AUTO_CREATE)
    }

    /**
     * Unbind MyService class to this activity when it pause.
     */
    override fun onPause() {
        super.onPause()
        unbindService(this)
    }

    /**
     * Access service when a connection builds between two.
     */
    override fun onServiceConnected(name: ComponentName, binder: IBinder) {
        val b = binder as MyService.MyBinder
        myService = b.service
    }

    /**
     * Set service object to null when service disconnected.
     */
    override fun onServiceDisconnected(name: ComponentName) {
        myService = null
    }

    /**
     * Called when user clicks "editTextFrom" widget
     */
     fun onEditTextFrom(v: View) {
         // Show date picker when user clicks editTextFrom. It takes current date.
         calendarFrom = Calendar.getInstance()
         val day = calendarFrom!!.get(Calendar.DAY_OF_MONTH)
         val month = calendarFrom!!.get(Calendar.MONTH)
         val year = calendarFrom!!.get(Calendar.YEAR)
         showDatePickerDialog(calendarFrom!!,day,month,year,editTextFrom)
    }

    /**
     * Called when user clicks "editTextTo" widget
     */
    fun onEditTextTo(v: View) {
        // Show date picker when user clicks editTextTo. It takes current date.
        calendarTo = Calendar.getInstance()
        val day = calendarTo!!.get(Calendar.DAY_OF_MONTH)
        val month = calendarTo!!.get(Calendar.MONTH)
        val year = calendarTo!!.get(Calendar.YEAR)
        showDatePickerDialog(calendarTo!!, day, month, year, editTextTo)
    }

    private fun showDatePickerDialog(calendar: Calendar, day: Int, month: Int, year: Int, editText: EditText) {
        // Display dialog with current date. When user selects any, show it in edit text.
        datePickerDialog = DatePickerDialog(this@MainActivity,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    editText!!.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year)
                    setCalendar(calendar,view)
                }, year, month, day)
        datePickerDialog!!.show()
    }

    /**
     * Set selected date in calendar.
     */
    private fun setCalendar(calendar: Calendar?, view: DatePicker) {
        calendar?.clear()
        calendar?.set(Calendar.MONTH, view.month)
        calendar?.set(Calendar.YEAR, view.year)
        calendar?.set(Calendar.DATE, view.dayOfMonth)
    }

    /**
     * Called when user clicks "Result" button
     */
    fun onButtonGetResult(v: View) {
        // Check for invalid date.
        // Invalid date -> If From date is greater than To date
        // or From date is equal to To date
        // or From date or To date not selected.
        if (isInvalidDate()){
            textViewResult.text = getString(R.string.invalid_date)
        }
        else {
            getWeekDays()
        }
    }

    /**
     * Algorithm: 1. First we find the no of days between From and To date.
     * 2. As we have to exclude selected date, we would subtract 2 from the total days.
     * 3. Our whole logic is based on To date, so we would save To date in seperate calendar object.
     * 4. If total no of days is completely divisible by 7, then atleast it has one weekend (Saturday/Sunday)
     * 5. Else if total days is greater than 6, then calculating till multiple of 7 and using quotient to find rest of the dates
     * for which we have to find whether the dates are on Saturday/Sunday.
     * 6. Say, total days are 20, then there are atleast 10 working days and we have to search days for last 6 dates.
     * 7. If the rest of the days are weekdays, then add day in total weekdays.
     * 8 If total no of days is less than 7, then quotient is the weekdays when divided by 7.
     */
    private fun getWeekDays() {
        val msDiff = calendarTo!!.timeInMillis - calendarFrom!!.timeInMillis
        val daysDiff = (TimeUnit.MILLISECONDS.toDays(msDiff) + 1).toInt() - 2

        calendarToCalculate = calendarTo!!.clone() as Calendar

        weekdays = 0

        if (daysDiff % 7 == 0) {
            weekdays = daysDiff - 2 * (daysDiff / 7)
        } else if (daysDiff > 6) {
            val v1 = daysDiff % 7
            weekdays = daysDiff / 7 * 5
            for (i in 0 until v1) {
                calendarToCalculate.add(Calendar.DATE, -1)

                when (calendarToCalculate.get(Calendar.DAY_OF_WEEK)) {
                    Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY, Calendar.THURSDAY, Calendar.FRIDAY -> weekdays += 1
                }
            }
        } else {
            weekdays = daysDiff
        }
        textViewResult.text = getString(R.string.there_are) + " " + weekdays.toString() + " "+ getString(R.string.weekdays)
    }

    /**
     * Check for invalid date.
     */
    private fun isInvalidDate(): Boolean {
        if (calendarFrom == null || calendarTo == null) {
            return true
        }
        else if((calendarFrom!!.compareTo(calendarTo) > 0) || (calendarFrom!!.compareTo(calendarTo) == 0)){
            return true
        }
        return false
    }

    /**
     * Get the list of public holidays from database, which is accessed by Service.
     */
    fun onButtonGetHolidays(v: View) {
        // Check if service is connected. If yes, then fetch the list of holidays from database and update list.
        year = editTextYear.text.toString()

        if (myService != null) {
            myService!!.delete()
            myService!!.insert(HolidayPresenter.getHolidays(year!!))

            holidayList!!.clear()
            holidayList!!.addAll(myService!!.addResultValues()!!)
            arrayAdapter!!.notifyDataSetChanged()
        }
    }
}
