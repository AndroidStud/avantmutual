package com.androidtest.avantmutual

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import java.util.ArrayList

/**
 * This class is the local service running in background which fetches and insert data from database and send it to MainActivity.
 */
class MyService : Service() {
    private val mBinder = MyBinder()
    var holidayList: List<String>? = null

    /**
     * Called as soon as service starts.
     */
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        return Service.START_NOT_STICKY
    }

    /**
     * Binds the service to MainActivity class.
     */
    override fun onBind(intent: Intent): IBinder? {
        return mBinder
    }

    inner class MyBinder : Binder() {
        internal val service: MyService
            get() = this@MyService
    }

    /**
     * Fetches data from database.
     */
    fun addResultValues(): List<String>? {
        val myDBHelper = MyDBHelper(applicationContext)
        holidayList = myDBHelper.getHolidayList(applicationContext)
        return holidayList
    }

    /**
     * Insert data to database.
     */
    fun insert(name: ArrayList<String>?) {
        val db = MyDBHelper(applicationContext)
        db.insert(name,applicationContext)
    }

    /**
     * Insert data from database.
     */
    fun delete() {
        val db = MyDBHelper(applicationContext)
        db.delete(applicationContext)
    }
}
