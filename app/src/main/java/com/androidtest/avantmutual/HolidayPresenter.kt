package com.androidtest.avantmutual

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Calendar
import java.util.Date

/**
 * This class is a helper class which calculates all sort of dates.
 * 1. Fixed dates(fixedHoliday) -> Anzac day (25/4)
 * 2. Dynamic dates/Monday holidays(allMondayHoliday) -> Public holidays when they fall on weekends, so next Monday becomes holiday.
 * New year day (01/01)
 * Australia day (26/01)
 * Christmas day (25/12)
 * Boxing day (26/12)
 * 3. Random public holidays(dynamicHoliday) -> Which falls on specific day in a month like 1st monday or 2nd monday.
 * Queens birthday(2nd monday in june)
 * Labour day(1st monday in october)
 * Except easter holidays rest are handled as Eater depends on astronomical events that.
 */

object HolidayPresenter {
    private var concatenatedHolidayList: ArrayList<String>? = null
    private var allMondayHoliday: ArrayList<String>? = null
    private var dynamicHoliday: ArrayList<String>? = null
    private var fixedHoliday: ArrayList<String>? = null

    fun getHolidays(year: String): ArrayList<String> {
        // Concatenated holiday list contains all the three types of holiday.
        concatenatedHolidayList = ArrayList()

        concatenatedHolidayList!!.clear()

        fixedHoliday = ArrayList()
        //Anzac day
        fixedHoliday!!.add("25/4/$year")

        allMondayHoliday = ArrayList()
        //new year day
        allMondayHoliday!!.add("01/01/$year")
        //australia day
        allMondayHoliday!!.add("26/01/$year")
        //christmas day
        allMondayHoliday!!.add("25/12/$year")
        //boxing day
        allMondayHoliday!!.add("26/12/$year")

        dynamicHoliday = ArrayList()

        getAllMondayHolidays(allMondayHoliday!!)
        getAllDynamicHolidays(year)

        concatenatedHolidayList!!.addAll(allMondayHoliday!!)
        concatenatedHolidayList!!.addAll(fixedHoliday!!)
        concatenatedHolidayList!!.addAll(dynamicHoliday!!)

        return concatenatedHolidayList as ArrayList<String>
    }

    private fun getAllDynamicHolidays(year: String) {
        //queens birthday
        //2nd monday in june
        getCustomDate(2, Calendar.MONDAY, Calendar.JUNE, year)

        //labour day
        //1st monday of october
        getCustomDate(1, Calendar.MONDAY, Calendar.OCTOBER, year)
    }

    private fun getCustomDate(weekInMonth: Int, day: Int, month: Int, year: String) {
        val cal = Calendar.getInstance()
        cal.set(Calendar.DAY_OF_WEEK, day)
        cal.set(Calendar.DAY_OF_WEEK_IN_MONTH, weekInMonth)
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.YEAR, Integer.parseInt(year))

        val date = cal.get(Calendar.DATE).toString() + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR)
        dynamicHoliday!!.add(date)
    }

    private fun getAllMondayHolidays(allMondayHoliday: ArrayList<String>) {
        val df = SimpleDateFormat("dd/MM/yyyy")

        for (i in allMondayHoliday.indices) {
            var readDate: Date? = null
            try {
                readDate = df.parse(allMondayHoliday[i])
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            val cal = Calendar.getInstance()
            cal.timeInMillis = readDate!!.time

            when (cal.get(Calendar.DAY_OF_WEEK)) {
                Calendar.SATURDAY -> cal.add(Calendar.DAY_OF_MONTH, 2)
                Calendar.SUNDAY -> cal.add(Calendar.DAY_OF_MONTH, 1)
            }

            allMondayHoliday.removeAt(i)
            val date = cal.get(Calendar.DATE).toString() + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR)

            allMondayHoliday.add(i, date)
        }
    }
}
